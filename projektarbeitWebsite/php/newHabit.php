<?php
    include_once 'header.php';
    include_once 'db/databaseConnection.php';
?>

    <script type="text/javascript">

        function selectButton(clickedButton, otherButton){
            document.getElementById(clickedButton).setAttribute("class", "sizeicon btn buttonActivated");
            document.getElementById(otherButton).setAttribute("class", "sizeicon btn buttonDeactivated");

//            if(clickedButton == "priorityButton"){
//                switchVisibility('prioritySelect', 'categorySelect');
//            } else  if(clickedButton == "categoryButton"){
//                switchVisibility('categorySelect', 'prioritySelect');
//            }
        }

//        function switchVisibility(elementToBeEnabled, elementToBeDisabled){
//            document.getElementById(elementToBeDisabled).setAttribute("disabled", "disabled");
//            document.getElementById(elementToBeEnabled).removeAttribute("disabled");
//        }
    </script>

    <main class="container" >
        <div class="row" id="content">
                <h2 class="currentPageTitle">Add a new Habit</h2>
                <form name="newHabitForm" action="actions/newHabitToDb.php" method="post">
                    <label class="eingabefeld">Name of your habit: </label><br>
                    <input type="text" class="eingabe col-xs-12" name="title">
                    <br>

                    <label class="eingabefeld">Description: </label><br>
                    <textarea name="description" rows="4" cols="30" class="eingabe col-xs-12">Enter your description here...</textarea>
<!--                    <input type="text" class="eingabe col-md-6 col-xs-12" name="description">-->
                    <br>

                    <label class="eingabefeld">Repeats: </label><br>
                    <div class=" eingabe btn-group btn-group-justified">
                        <a href="#" class="sizeicon btn buttonActivated" id="dailyButton" onclick="selectButton('dailyButton', 'weeklyButton')" name="wiederholungsrate" value="7"><span class="glyphicon glyphicon-repeat"></span><br>daily</a>
                        <a href="#" class="sizeicon btn buttonDeactivated" id="weeklyButton" onclick="selectButton('weeklyButton', 'dailyButton')" name="wiederholungsrate" value="1"><span class="glyphicon glyphicon-calendar"></span><br>weekly</a>
<!--                        <option name="wiederholungsrate" value="daily" class="sizeicon btn buttonActivated">-->
<!--                        <option name="wiederholungsrate" value="weekly" class="sizeicon btn buttonDeactivated">-->
                    </div>

                    <label class="eingabefeld">Actions: </label><br>
                    <div class="eingabe btn-group btn-group-justified">
                        <a href="#" class="sizeicon btn buttonActivated" id="priorityButton" onclick="selectButton('priorityButton', 'categoryButton')"><span class="glyphicon glyphicon-exclamation-sign"></span><br>priority</a>
                        <a href="#" class="sizeicon btn buttonDeactivated" id="categoryButton" onclick="selectButton('categoryButton', 'priorityButton')"><span class="glyphicon glyphicon-inbox"></span><br>category</a>
                    </div>

                    <div class="row" id="newHabitTabs">
                        <div id="prioritySelection" class="col-xs-6 selectionTabsNewHabit">
                            <div id="prioritySelect">
                            <p class="prioCheckboxLine checkbox"><input type="radio" name="priority" value="1">  <label> 1</label></p>
                            <p class="prioCheckboxLine checkbox"><input type="radio" name="priority" value="2">  <label> 2</label></p>
                            <p class="prioCheckboxLine checkbox"><input type="radio" name="priority" value="3">  <label> 3</label></p>
                            <p class="prioCheckboxLine checkbox"><input type="radio" name="priority" value="4">  <label> 4</label></p>
                            <p class="prioCheckboxLine checkbox"><input type="radio" name="priority" value="5">  <label> 5</label></p>
                            </div>
                        </div>

                        <div id="categorySelection" class="col-xs-6 selectionTabsNewHabit">
<!--                            Kategorien aus DB auslesen + anzeigen + Button "Neue Kategorie"-->
                            <?php
                                checkConnection();

                                $sql = "SELECT * FROM Kategorie";
                                $result = $conn->query($sql);

                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    //Open list-group
                                    $htmlCategoryList = "<select id='categorySelect' name='categorySelect' class='col-xs-12 select-format form-control'>";
                                    while($row = $result->fetch_assoc()) {
                                        //Oeffne Zeile
                                        $htmlCategoryList = $htmlCategoryList . "<option";

                                        //Setze erste Kategorie auf 'active'
                                        if($row["kategorieId"] == '1'){
                                            $htmlCategoryList = $htmlCategoryList . " selected";
                                        }

                                        //Schliesse Zeile ab
                                        $htmlCategoryList = $htmlCategoryList . " value='".$row["kategorieId"]."'>".$row["titel"]."   (Prio: ".$row["prioritaet"].")</option>";
                                    }
                                    //Schliesse Liste und gib es aus
                                     echo $htmlCategoryList . "<option value='newCategory'><a href='newCategory.php'>+ Add new Category</a></option></select>";
                                } else {
                                    echo "0 results";
                                }
                                $conn->close();
                            ?>
                        </div>
                    </div>
<!--                    <button type="submit" id="ok" name="submitNewHabit">Speichern</button>-->
                    <input type="submit" name="submit" value="Speichern" class="btn buttonDeactivated col-xs-6 btn-lg">
                </form>
        </div>
    </main>


<?php include_once 'footer.php';?>