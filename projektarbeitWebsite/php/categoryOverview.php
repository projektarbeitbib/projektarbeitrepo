<?php
/**
 * Created by PhpStorm.
 * User: vanes
 * Date: 11.03.2018
 * Time: 23:45
 */

include_once 'header.php';
include_once 'db/databaseConnection.php';
?>
<main class="container" id="content">
    <h2 class="currentPageTitle">Category Overview</h2>
    <div id="row">
        <?php
        checkConnection();

        $sql = "SELECT kategorieId, titel FROM Kategorie";
        $resultCategories = $conn->query($sql);

        if ($resultCategories->num_rows > 0) {
            // output data of each row
            $htmlCategoryList = "<div id='categoryList' class='col-xs-8 col-xs-offset-2'>";
            while($row = $resultCategories->fetch_assoc()) {

                //Fuege Zeile hinzu
                $htmlCategoryList = $htmlCategoryList . "<a href='showCategory.php' class='btn habit buttonActivated'>".$row['titel']."</a>";

            }
            //Gib Liste aus:
            echo $htmlCategoryList . "</div>";
        } else {
            echo "no categories";
        }
        $conn->close();
        ?>
    </div>
</main>


<?php
include_once 'footer.php';
?>