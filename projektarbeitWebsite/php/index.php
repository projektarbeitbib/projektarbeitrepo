<?php
    include_once 'header.php';
    include_once  'db/databaseConnection.php';
?>

<script>
    function checkHabit(statusId){
        // Darstellung ändern
        var content = document.getElementById(statusId).innerHTML;
        var gewohnheitErfuellt;

        if(content == "X"){
            document.getElementById(statusId).innerHTML ="-";
            gewohnheitErfuellt = 0;
        } else{
            document.getElementById(statusId).innerHTML ="X";
            gewohnheitErfuellt = 1;
        }

        //Eintrag in DB ändern
//        alert(statusId +" und "+gewohnheitErfuellt);

        $.post('actions/checkHabit.php', {statusId: statusId, gewohnheitErfuellt: gewohnheitErfuellt},
            function (data) {
                alert(data);
            });
    }
</script>

<main class="container" id="content">
    <h2 class="currentPageTitle">Habit overview</h2>
    <div id="row">
        <div class="row" id="habitOverview">

            <div class="col-xs-8 col-xs-offset-4" id="datumreihe">
                <span class="dateDay col-xs-1"><?php echo date("d") ?></span>
                <span class="dateDay col-xs-1"><?php echo date("d", strtotime('-1 days')) ?></span>
                <span class="dateDay col-xs-1"><?php echo date("d", strtotime('-2 days')) ?></span>
                <span class="dateDay col-xs-1"><?php echo date("d", strtotime('-3 days')) ?></span>
                <span class="dateDay col-xs-1"><?php echo date("d", strtotime('-4 days')) ?></span>
                <span class="dateDay col-xs-1"><?php echo date("d", strtotime('-5 days')) ?></span>
                <span class="dateDay col-xs-1"><?php echo date("d", strtotime('-6 days')) ?></span>
                <span class="dateDay col-xs-1"><?php echo date("d", strtotime('-7 days')) ?></span>
                <span class="dateDay col-xs-1"><?php echo date("d", strtotime('-8 days')) ?></span>
                <span class="dateDay col-xs-1"><?php echo date("d", strtotime('-9 days')) ?></span>
                <span class="dateDay col-xs-1"><?php echo date("d", strtotime('-10 days')) ?></span>
                <span class="dateDay col-xs-1"><?php echo date("d", strtotime('-11 days')) ?></span>
            </div>




            <div id="listOfHabits" class="col-xs-4">
                <?php
                checkConnection();

                $sql = "SELECT gewohnheitsId, titel FROM Gewohnheit";
                $resultHabits = $conn->query($sql);



                if ($resultHabits->num_rows > 0) {
                    // output data of each row
                    $htmlHabitList = "";
                    $htmlHabitCheckboxes = "<div id='checkboxesHabit' class='col-xs-8'>";
                    while($row = $resultHabits->fetch_assoc()) {
                        //Frage Stati dieses Habits ab:
                        $sql = "SELECT statusId, gewohnheitErfuellt FROM Status WHERE Status.gewohnheitsId = ".$row['gewohnheitsId']
                            ." AND datum BETWEEN ".date('yyyy-mm-dd', strtotime('-11 days'))." AND ".date('yyyy-mm-dd').";";
                        $resultStatus = $conn->query($sql);


                        //Fuege Zeile hinzu
                        $htmlHabitList = $htmlHabitList . "<a href='showHabit.php' class='btn habit buttonActivated'>".$row['titel']."</a>";
                        $htmlHabitCheckboxes = $htmlHabitCheckboxes
                            . "<div class=\"habitCheckRow\">
                                <form>";

                        while($statusRow = $resultStatus->fetch_assoc()){
                            if($statusRow["gewohnheitErfuellt"] == 1){
                                $checkmark = "X";
                            } else{
                                $checkmark = "-";
                            }
                            $htmlHabitCheckboxes = $htmlHabitCheckboxes. "<button id='".$statusRow["statusId"]."' type=\"button\" onclick=\"checkHabit(this.id)\" class=\"col-xs-1 habitCheckbox\">".$checkmark."</button>";
                        }

                        $htmlHabitCheckboxes = $htmlHabitCheckboxes ."</form>
                            </div>";
                    }
                    //Gib Liste aus:
                    echo $htmlHabitList . "</div>" . $htmlHabitCheckboxes . "</div>";
                } else {
                    echo "no habits";
                }
                $conn->close();
                ?>
        </div>
    </div>
</main>


<?php include_once 'footer.php';?>