<?php
    include_once 'header.php';
    include_once  'db/databaseConnection.php';
?>

<script>
    function createNewCategory(){
        var title = document.getElementById('category-title').value;
        var priorityOptions = document.getElementsByName('category-priority');
        var priority, i;
        for (i = 0; i < priorityOptions.length; i++) {
            if (priorityOptions[i].checked) {
                priority = priorityOptions[i].value;
            }
        }

//        alert(""+title+" has prio "+ priority);

        $.post('actions/newCategoryToDb.php', {title:title, priority:priority},
            function (data) {
                alert(data);
            });

//        $.ajax({
//            type: "POST",
//            url: 'actions/newCategoryToDb.php',
//            dataType: 'json',
//            data: {
//                functionname: 'addNewCategory',
//                arguments: [title, priority]
//            },
//            success: function (data) {
//                alert(data);
//            }
//        });
    }
</script>

<main class="container" >
    <div class="row" id="content">
        <h2 class="currentPageTitle">Add a new Category</h2>

        <form class="col-xs-8 col-xs-offset-2">
            <label class="eingabefeld">Category Title: </label><br>
            <input type="text" class="eingabe col-xs-12" name="category-title" id="category-title">

            <label class="eingabefeld">Priority: </label><br>

            <div class="col-xs-12 buttonActivated">
                <span class="col-xs-1 col-xs-offset-2">
                    <input id="prio1" type="radio" name="category-priority" value="1" class="custom-checkbox">
                    <label for="prio1"> 1</label>
                </span>
                <span class="col-xs-1 col-xs-offset-1">
                    <input id="prio2" type="radio" name="category-priority" value="2" class="custom-checkbox">
                    <label for="prio2"> 2</label>
                </span>
                <span class="col-xs-1 col-xs-offset-1">
                    <input id="prio3" type="radio" name="category-priority" value="3" class="custom-checkbox" checked>
                    <label for="prio3" > 3</label>
                </span>
                <span class="col-xs-1 col-xs-offset-1">
                    <input id="prio4" type="radio" name="category-priority" value="4" class="custom-checkbox">
                    <label for="prio4"> 4</label>
                </span>
                <span class="col-xs-1 col-xs-offset-1">
                    <input id="prio5" type="radio" name="category-priority" value="5" class="custom-checkbox">
                    <label for="prio5"> 5</label>
                </span>
            </div>

                <input type="submit" name="category-submit" value="Submit"
                       onclick="createNewCategory()"
                       class="btn buttonDeactivated col-xs-6 margin-top-20px">
        </form>
    </div>
</main>

<?php
include_once 'footer.php';?>