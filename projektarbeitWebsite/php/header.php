<!DOCTYPE html>
<html lang="en">
<head>
    <title>Habittracker</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC" rel="stylesheet">
    <link rel="stylesheet/less" type="text/css" href="../css/styles.less" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.0.0/less.min.js" ></script>
    <script type="text/javascript"  src="../jquery/jquery.js"></script>
    <!--<script src="jquery\jquery.js"></script>-->
</head>
<body class="amaticSC">
<header>
        <div id="header-image">

        </div>

    <!-- Navigation -->
    <nav class="nav navbar-inverse nav-justified">
        <div class="row">
            <div class="col-lg-3 col-xs-12">
                <ul class="nav navbar-nav">
                    <li role="presentation" class="dropdown">
                        <a class="dropdown-toggle glyphicon glyphicon-menu-hamburger" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false"><span class="amaticSC"> Menu</span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php">My Habits</a></li>
                            <li><a href="categoryOverview.php">My Categories</a></li>
                            <li><a href="#">Settings</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

<!--            <div class="col-lg-6 col-xs-12">-->
<!--                <ul class="nav navbar-nav">-->
<!--                    <li><a href="index.php" class="glyphicon glyphicon-home"></a></li>-->
<!--                </ul>-->
<!--            </div>-->

            <div class="col-lg-offset-6 col-lg-3 col-xs-12 text-align-center">
                <div class="row">
                    <ul class="nav navbar-nav col-lg-12" id="navigation-right">
                        <li role="presentation" class="dropdown col-lg-6 col-xs-12">
                            <a href="#" class="dropdown-toggle glyphicon glyphicon-align-left" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false"><span class="amaticSC"> Sort</span></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><a href="#">By Category</a></li>
                                <li class="dropdown-item"><a href="#">By Priority</a></li>
                            </ul>
                        </li>
<!--                        <li class="col-lg-6 col-xs-12"><a href="newHabit.php" class="glyphicon glyphicon-plus-sign"><span class="amaticSC"> Add</span></a></li>-->
                        <li role="presentation" class="dropdown show col-lg-6 col-xs-12">
                            <a href="#" class="dropdown-toggle glyphicon glyphicon-plus-sign" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false"><span class="amaticSC"> Add</span></a>

                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><a href="newHabit.php">New Habit</a></li>
                                <li class="dropdown-item"><a href="newCategory.php">New Category</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

</header>