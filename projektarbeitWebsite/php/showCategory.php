<?php
/**
 * Created by PhpStorm.
 * User: vanes
 * Date: 11.03.2018
 * Time: 23:52
 */

    include_once 'header.php';
    include_once 'db/databaseConnection.php';
?>

<main class="container" id="content">
    <h2 class="currentPageTitle">Category Detail View</h2>
    <h3 id="secondPageTitle">
        <?php
            checkConnection();
            $kategorieId = 1;
            $sql = "SELECT titel FROM Kategorie WHERE Kategorie.kategorieId = ".$kategorieId;
            $result = $conn->query($sql);
            $row = $result->fetch_assoc();

            echo $row["titel"];
        ?>
    </h3>

    <div class="row">
        <div class="col-xs-offset-2 col-xs-8">
        <div class="buttonDeactivated col-xs-12" id="showCategory-priority">
            <?php
                checkConnection();

                $sql = "SELECT prioritaet FROM Kategorie WHERE Kategorie.kategorieId = ".$kategorieId;
                $result = $conn->query($sql);
                $row = $result->fetch_assoc();

                echo $row["prioritaet"];
            ?>
        </div>

        <div id="listOfHabits" class="col-xs-12">
            <?php
            checkConnection();

            $sql = "SELECT gewohnheitsId, titel FROM Gewohnheit WHERE Gewohnheit.kategorieId = ".$kategorieId;
            $resultHabits = $conn->query($sql);



            if ($resultHabits->num_rows > 0) {
                // output data of each row
                $htmlHabitList = "";
                while($row = $resultHabits->fetch_assoc()) {

                    //Fuege Zeile hinzu
                    $htmlHabitList = $htmlHabitList . "<a href='showHabit.php' class='btn habit buttonActivated'>".$row['titel']."</a>";

                }
                //Gib Liste aus:
                echo $htmlHabitList . "</div>";
            } else {
                echo "no habits";
            }
            $conn->close();
            ?>
        </div>
    </div>
</main>


<?php
    include_once 'footer.php';
?>
