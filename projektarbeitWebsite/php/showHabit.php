<?php
/**
 * Created by PhpStorm.
 * User: vanes
 * Date: 10.03.2018
 * Time: 10:35
 */
    include_once 'header.php';
    include_once 'db/databaseConnection.php';
?>

<script>
    function changeMonth(direction){}

        $(document).ready(function(){

            $('#nextMonthButton').click(function(){

                $.ajax({
                    type: 'post',
                    data: {direction: "+1"},
                    success: function (data) {
                        alert(data);
                    }
                });
            });

            $('#previousMonthButton').click(function(){

                $.ajax({
                    type: 'post',
                    data: {direction: "-1"},
                    success: function (data) {
                        alert(data);
                    }
                });
            });
        });

</script>

<main class="container" id="content">
    <h2 class="currentPageTitle">Habit Detail View</h2>
    <h3 id="secondPageTitle">
        <?php
        $gewohnheitsId = 5;

        checkConnection();

        $sql = "SELECT titel FROM Gewohnheit WHERE Gewohnheit.gewohnheitsId = ".$gewohnheitsId.";";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();

        echo $row["titel"];
        ?>
    </h3>
    <div id="row">

        <div id="showHabit-description" class="col-xs-12">
            <?php
            checkConnection();

            $sql = "SELECT beschreibung FROM Gewohnheit WHERE Gewohnheit.gewohnheitsId = ".$gewohnheitsId.";";
            $result = $conn->query($sql);
            $row = $result->fetch_assoc();

            echo $row["beschreibung"];
            ?>
        </div>

        <div id="calendar" class="col-lg-6 col-xs-12">
            <div id="month">
                <div id="nextMonthButton" class="glyphicon glyphicon-menu-left col-xs-2 arrow"></div>
                <div class="col-xs-8">
                    <?php
                    checkConnection();
                        $currentlyDispayedMonth = 0;

                        if( isset($_POST['direction']) ) {
                            $direction = $_POST['direction'];
                            changeMonth($direction);
                        }
                        function changeMonth($todirection){
                            $GLOBALS['currentlyDispayedMonth'] = $GLOBALS['currentlyDispayedMonth'] + $todirection;
                            echo date("F Y", strtotime($GLOBALS['currentlyDispayedMonth']." months"));
                        }

                        echo date("F Y", strtotime($currentlyDispayedMonth." months"));
                    ?>
                </div>
                <div id="previousMonthButton" class="glyphicon glyphicon-menu-right col-xs-2 arrow"></div>
            </div>

            <div id="weekdays">
                <ul>
                    <li>Mo</li>
                    <li>Tu</li>
                    <li>We</li>
                    <li>Th</li>
                    <li>Fr</li>
                    <li>Sa</li>
                    <li>Su</li>
                </ul>
            </div>

            <div id="days">
                <ul>
                    <?php
                        // Check connection
                        checkConnection();

                        $sql = "SELECT gewohnheitErfuellt, datum FROM Status WHERE Status.gewohnheitsId = ".$gewohnheitsId." AND datum LIKE '".date("Y-m", strtotime($currentlyDispayedMonth." months"))."%' ORDER BY datum;";
                        $resultCheckedDays = $conn->query($sql);

                        $month = date("m", strtotime($currentlyDispayedMonth." months"));
                        $year = date("Y", strtotime($currentlyDispayedMonth." months"));
                        $numberOfDaysThisMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);

                        $firstWeekdayOfTheMonth = date("D", strtotime(date("d", strtotime($currentlyDispayedMonth.' months -1 day')).' days'));
                        $numberOfEmptyDaysToAdd = 0;
                        switch($firstWeekdayOfTheMonth){
                            case "Tue": $numberOfEmptyDaysToAdd = 1; break;
                            case "Wed": $numberOfEmptyDaysToAdd = 2; break;
                            case "Thu": $numberOfEmptyDaysToAdd = 3; break;
                            case "Fri": $numberOfEmptyDaysToAdd = 4; break;
                            case "Sat": $numberOfEmptyDaysToAdd = 5; break;
                            case "Sun": $numberOfEmptyDaysToAdd = 6; break;
                        }

                        $htmlOutput = "";

                        for($i = 0; $i < $numberOfEmptyDaysToAdd; $i++){
                            $htmlOutput = $htmlOutput ."<li>  </li>";
                        }

                        while($rowDay = $resultCheckedDays->fetch_assoc()){
                            $htmlOutput = $htmlOutput . "<li";

                            if($rowDay["gewohnheitErfuellt"] == 1){
                                $htmlOutput = $htmlOutput . " class='buttonActivated'";
                            }

                            $htmlOutput = $htmlOutput . ">". substr($rowDay["datum"], -2)."</li>";
                        }

                        for($i = $resultCheckedDays->num_rows + 1; $i <= $numberOfDaysThisMonth; $i++){
                            $htmlOutput = $htmlOutput . "<li>".$i."</li>";
                        }
                        echo $htmlOutput;
                    ?>
                </ul>
            </div>
        </div>

        <div id="statistics" class="col-lg-6 col-xs-12">
            <p class="statistics-block">
                Completed </br>
                <span class="percentage">
                <?php
                    // Check connection
                    checkConnection();

                    $sql = "SELECT erstelldatum FROM Gewohnheit WHERE Gewohnheit.gewohnheitsId = ".$gewohnheitsId;
                    $resultErstelldatum = $conn->query($sql);
                    $row = $resultErstelldatum->fetch_assoc();
                    $resultErstelldatum = date_create($row["erstelldatum"]);

                    $currentDate = date_create(date("Y-m-d"));
                    $dateDifference = date_diff($resultErstelldatum,$currentDate);
                    $dateDifference = $dateDifference->format("%a");
                    $dateDifference = $dateDifference + 1;

                    $sql = "SELECT COUNT(gewohnheitErfuellt) AS erfuellt FROM Status WHERE Status.gewohnheitsId = ".$gewohnheitsId ." AND Status.gewohnheitErfuellt = 1;";
                    $resultNumberOfCheckedDays = $conn->query($sql);
                    $row2 = $resultNumberOfCheckedDays->fetch_assoc();
                    $resultNumberOfCheckedDays = $row2["erfuellt"];

                    if($dateDifference == 0){
                        $percentage = 0;
                    } else {
                        $percentage = $resultNumberOfCheckedDays / $dateDifference * 100;
                    }
                    echo number_format($percentage, 2) . "%";
//                    $conn->close();
                ?>
                </span> </br>
                of the time!
            </p>

            <p class="statistics-block">
                <span class="percentage">
                    <?php
                    // Check connection
                    checkConnection();

                    $sql = "SELECT COUNT(gewohnheitErfuellt) AS tageErfuellt FROM Status WHERE Status.gewohnheitsId = ".$gewohnheitsId ." AND Status.gewohnheitErfuellt = 1 AND datum LIKE '".date("Y-m")."%';";
                    $resultCurrentMonth = $conn->query($sql);
                    $row = $resultCurrentMonth->fetch_assoc();
                    $resultCurrentMonth = $row["tageErfuellt"];
                    $resultCurrentMonth = $resultCurrentMonth / cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));

                    $sql = "SELECT COUNT(gewohnheitErfuellt) AS tageErfuellt FROM Status WHERE Status.gewohnheitsId = ".$gewohnheitsId ." AND Status.gewohnheitErfuellt = 1 AND datum LIKE '".date("Y-m", strtotime('-1 months'))."%';";
                    $resultLastMonth = $conn->query($sql);
                    $row = $resultLastMonth->fetch_assoc();
                    $resultLastMonth = $row["tageErfuellt"];
                    $resultLastMonth = $resultLastMonth / cal_days_in_month(CAL_GREGORIAN, date("m", strtotime('-1 months')), date("Y"));

                    $percentage =  $resultCurrentMonth - $resultLastMonth;

                    echo number_format($percentage, 2) . "%";
//                    $conn->close();
                    ?>
                </span> </br>
                Progress to last month!
            </p>

            <p class="statistics-block">
                <span class="percentage">
                    <?php
                    // Check connection
                    checkConnection();

                    $sql = "SELECT COUNT(gewohnheitErfuellt) AS tageErfuellt FROM Status WHERE Status.gewohnheitsId = ".$gewohnheitsId ." AND Status.gewohnheitErfuellt = 1 AND datum LIKE '".date("Y")."%';";
                    $resultCurrentYear = $conn->query($sql);
                    $row = $resultCurrentYear->fetch_assoc();
                    $resultCurrentYear = $row["tageErfuellt"];
                    $resultCurrentYear = $resultCurrentMonth / cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));

                    $sql = "SELECT COUNT(gewohnheitErfuellt) AS tageErfuellt FROM Status WHERE Status.gewohnheitsId = ".$gewohnheitsId ." AND Status.gewohnheitErfuellt = 1 AND datum LIKE '".date("Y", strtotime('-1 year'))."%';";
                    $resultLastYear = $conn->query($sql);
                    $row = $resultLastYear->fetch_assoc();
                    $resultLastYear = $row["tageErfuellt"];
                    $resultLastYear = $resultLastMonth / cal_days_in_month(CAL_GREGORIAN, date("m", strtotime('-1 months')), date("Y"));

                    $percentage = $resultCurrentYear - $resultLastYear ;

                    echo number_format($percentage, 2) . "%";
                    $conn->close();
                    ?>
                </span> </br>
                Progress to last year!
            </p>
        </div>

       </div>
</main>

<?php
    include_once 'footer.php';
?>
