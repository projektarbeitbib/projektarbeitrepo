<?php
/**
 * Created by PhpStorm.
 * User: vanes
 * Date: 08.03.2018
 * Time: 12:45
 */

include_once '../db/databaseConnection.php';

//if(isset($_POST['submit'])){
    $titel = $_POST["title"];
    $beschreibung = $_POST["description"];
    $prioritaet = $_POST["priority"];
    $wiederholungsrate = $_POST["wiederholungsrate"];
    $wiederholungszeitraum = 7;
    $kategorieId = $_POST["categorySelect"];

    //Eingabe prüfen:

    if(empty($titel)){
        $titleErr = "Title is required!";
    } else{
        if (!preg_match("/^[a-zA-Z0-9 ]*$/", $titel)) {
            $titleErr = "Only letters and white space allowed";
        }
    }

    if(empty($beschreibung)){
        $beschreibung = "";
    }

    if(empty($prioritaet)){
        $prioritaet = 3;
    } else{
        if (!preg_match("/^[1-5]*$/",$prioritaet)) {
            $priorityErr = "Only numbers from 1 to 5 allowed";
        }
    }

    if(empty($wiederholungsrate)){
        $wiederholungsrate = 1;
    } else{
        if (!preg_match("/^[17]*$/",$wiederholungsrate)) {
            $repititionErr = "Only 1 or 7 allowed";
        }
    }

    if(empty($kategorieId)){
        $categoryErr = "CategoryId is required!";
        $kategorieId = 1;
    } else{
        if (!preg_match("/^[1-9]*$/",$kategorieId)) {
            $categoryErr = "Only numbers allowed";
        }
    }




    $sql = "INSERT INTO Gewohnheit (titel, beschreibung, prioritaet, wiederholungsrate, wiederholungszeitraum, kategorieId)
    VALUES ('". $titel ."', '". $beschreibung ."', '". $prioritaet . "', '". $wiederholungsrate ."', '".$wiederholungszeitraum. "', '".$kategorieId."')";

    if ($conn->query($sql) === TRUE) {
        echo "New habit created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $conn->close();
//} else {
//    echo "SPEICHERN WURDE NICHT GEKLICKT!";
//}


