<?php
/**
 * Created by PhpStorm.
 * User: vanes
 * Date: 08.03.2018
 * Time: 12:45
 */

include "../db/databaseConnection.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    //Eingabe prüfen:
    if (isset($_POST['title'])) {
        $title = $_POST['title'];
    } else {
        $title = "Ich bin leer";
        echo "Title is required!";
    }

    if (isset($_POST['priority'])) {
        $priority = $_POST['priority'];
        if (!preg_match("/^[1-5]*$/", $priority)) {
            echo "Only numbers from 1 to 5 allowed";
        }
    } else {
        $priority = 3;
        echo "Priority required!";
    }


    $sql = "INSERT INTO Kategorie (titel, prioritaet)
        VALUES ('" . $title . "', '" . $priority . "')";

    if (!isset($conn)) {
        echo "conn nicht eingebunden";
    } else {
        echo "Verbindung steht!";
    }

    if ($conn->query($sql) === TRUE) {
        echo "New category created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $conn->close();

} else{
    echo  "method not post";
}
